package com.example.imagepicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.kroegerama.imgpicker.BottomSheetImagePicker;
import com.kroegerama.imgpicker.ButtonType;

import java.util.List;

public class MainActivity extends AppCompatActivity implements BottomSheetImagePicker.OnImagesSelectedListener {
    private Button btnImagePick;
    private ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnImagePick = findViewById(R.id.btn_image_pick);
        iv = findViewById(R.id.iv);

        btnImagePick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new BottomSheetImagePicker.Builder(getString(R.string.file_provider))
                        .cameraButton(ButtonType.Button)
                        .galleryButton(ButtonType.Button)
                        .singleSelectTitle(R.string.pick_single)
                        .requestTag("single")
                        .show(getSupportFragmentManager(), null);
            }
        });
    }

    @Override
    public void onImagesSelected(List<? extends Uri> list, String s) {
        for (Uri uri : list) {
            Glide.with(this).load(uri).into(iv);
        }
    }
}